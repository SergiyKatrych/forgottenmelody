﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AppManager : MonoBehaviour {

	#region Simple Singletone

	private static AppManager _instance;

	public static AppManager Instance
	{
		get { return _instance; }
	}

	private void Awake()
	{
		if (_instance != null && _instance != this)
		{
			Debug.Log("Destroyed");
			Destroy(this.gameObject);
		}
		else
		{
			_instance = this;
			Debug.Log("Init");
			_instance.Init();
			DontDestroyOnLoad(_instance);
		}
	}

	#endregion

	public List<string> scenesList;
	public int currentSceneIndex = 0;
	
	public PartyFader _partyFader;
	 
	private void Init()
	{
		currentSceneIndex = 0;
	}

	public void StartWeeked()
	{
		_partyFader.StartNight(OnNightFaded);
	}

	public void OnNightFaded()
	{
		StartCoroutine(LoadNextScene());
	}

	private IEnumerator LoadNextScene()
	{
		AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(scenesList[++currentSceneIndex]);

		// Wait until the asynchronous scene fully loads
		while (!asyncLoad.isDone)
		{
			yield return null;
		}

		_partyFader.StartFadeOutNight();
	}
}
