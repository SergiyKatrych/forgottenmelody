﻿using UnityEngine;
using UnityEngine.UI;

public class BacgroundControllers : MonoBehaviour
{
	public Image realityImage, fantasyImage;

	public Sprite realitySprite, fantasySprite;

	public ParticleSystem particles;
	private Color fantasyColor;


	[Range(0, 1f)] public float fantasyProgress;
	private float progressOfOnePhrase;

	public bool isCompleted = false;

	public void SetCompletedLevel()
	{
		fantasyProgress = 1;
		particles.Play();
		isCompleted = true;
	}


	void Update()
	{
		fantasyColor.a = Mathf.Lerp(fantasyImage.color.a, fantasyProgress, Time.deltaTime);
		fantasyImage.color = fantasyColor;
	}

	public void Init(int partsCount, Sprite realitySprt, Sprite fantasySprt)
	{
		isCompleted = false;
		progressOfOnePhrase = 1 / (float) partsCount;
		realitySprite = realitySprt;
		realityImage.sprite = realitySprite;

		fantasySprite = fantasySprt;
		fantasyImage.sprite = fantasySprite;
		fantasyColor = fantasyImage.color;
		fantasyColor.a = 0;
		fantasyImage.color = fantasyColor;
	}

	public void OnMelodyPartIsPlay(bool isCorrectPart)
	{
		if (!isCompleted)
		{
			if (isCorrectPart)
			{
				fantasyProgress += progressOfOnePhrase;
			}
			else
			{
				if (fantasyProgress > progressOfOnePhrase)
				{
					fantasyProgress -= progressOfOnePhrase;
				}
				else
				{
					fantasyProgress = 0;
				}
			}
		}
	}
}