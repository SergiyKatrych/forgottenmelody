﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DragableItem : MonoBehaviour
{
	private RectTransform _rectTransform;
	private Image _image;
	private bool isFollowingPointer;
	public float speed = 10;

	private MelodyController _melodyController;
	
	public event Action OnStartDragging;

	void Start()
	{
		OnStartDragging += () => { };
		_rectTransform = GetComponent<RectTransform>();
		_image = GetComponent<Image>();

		_melodyController = FindObjectOfType<MelodyController>();
	}

	public void OnPointerDown(BaseEventData eventData)
	{
		isFollowingPointer = true;
		_image.raycastTarget = false;
		
		_melodyController.OnStartDraggingPhrase(GetComponent<PhraseHolder>());
		OnStartDragging();
		OnStartDragging = null;
		OnStartDragging += () => { };
	}

	
	public void OnPointerUp(BaseEventData eventData)
	{
		if (isFollowingPointer)
		{
			isFollowingPointer = false;
			_image.raycastTarget = true;
			_rectTransform.position = Input.mousePosition;
			_melodyController.OnEndDraggingPhrase();

			if (melodyHolder != null)
			{
				melodyHolder.OnPointerUp(this);
			}
		}
	}

	private PartMelodyHolder melodyHolder;
	public void OnEnterHolder(PartMelodyHolder holder)
	{
		melodyHolder = holder;
	}

	public void OnLeaveHolder(PartMelodyHolder holder)
	{
		melodyHolder = null;
	}

	public void Update()
	{
		if (isFollowingPointer)
		{
			_rectTransform.position = Vector2.Lerp(_rectTransform.position, Input.mousePosition, Time.deltaTime * speed);
		}
	}
}