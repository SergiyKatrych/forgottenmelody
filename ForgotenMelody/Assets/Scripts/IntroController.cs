﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class IntroController : MonoBehaviour
{
	public AudioClip practicing, pianoCrash;
	public AudioSource classicalSource;
	public CanvasGroup introImage;
	public Text fuckItText;

	public float fadeInSpeed = 0.1f;
	public float delayBeforeExplode = 10;
	
	private float targetVolume, targetAlpha;
	public float volumeChangeSpeed = 0.1f;
	
	// Use this for initialization
	void Awake ()
	{
		classicalSource.volume = 0;
		classicalSource.clip = practicing;
		fuckItText.gameObject.SetActive(false);
	}

	void Start()
	{
		
		targetVolume = 1;

		StartCoroutine(IntroSequence());
	}

	private IEnumerator IntroSequence()
	{
		targetVolume = 1;
		targetAlpha = 1;
		yield return new WaitForSeconds(delayBeforeExplode);
		
		classicalSource.Stop();
		classicalSource.clip = pianoCrash;
		classicalSource.loop = false;
		classicalSource.Play();
		yield return new WaitForSeconds(0.5f);
		introImage.gameObject.SetActive(false);
		yield return new WaitForSeconds(3f);
		fuckItText.gameObject.SetActive(true);
		yield return new WaitForSeconds(2f);
		fuckItText.gameObject.SetActive(false);
		yield return new WaitForSeconds(1f);
		AppManager.Instance.StartWeeked();
	}
	
	// Update is called once per frame
	void Update()
	{
		if (introImage.alpha != targetAlpha)
		{
			introImage.alpha = Mathf.Lerp(introImage.alpha, targetAlpha, Time.deltaTime * fadeInSpeed);
		}
		if (Mathf.Abs(volumeChangeSpeed - classicalSource.volume) > 0.05f)
		{
			classicalSource.volume = Mathf.Lerp(classicalSource.volume, targetVolume, Time.deltaTime * volumeChangeSpeed);
		}
	}
}
