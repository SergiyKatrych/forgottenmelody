﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class MelodyController : MonoBehaviour
{
	public Sprite realitySprite, fantasySprite;
	public int[] melody = new int[] {1, 2, 3, 4, 5, 6, 7};

	//First CLIP IS SILENT
	public List<AudioClip> melodyClips;
	public AudioClip fullMelody;

	public PhraseHolder phraseHolderPrefab;
	private RectTransform phrasesSpawnRect;

	private GridLayoutGroup melodyGridHolder;
	public PartMelodyHolder melodyPartPrefab;
	private List<PartMelodyHolder> _melodyParts;

	private AudioSource _melodyAudioSource;

	private Button playMelodyButton;

	[HideInInspector]
	public PhraseHolder currentlyPlayingPhrase;
	private BacgroundControllers backgroundController;
	private Image successBlocker;

	private float progressOfOnePhrase;

	private bool isMelodyCorrect = false;

	public void OnEnable()
	{
		Debug.Log("onEnable ");
		successBlocker = GameObject.Find("Blocker").GetComponent<Image>();
		successBlocker.gameObject.SetActive(false);
		_melodyAudioSource = GetComponent<AudioSource>();
		backgroundController = FindObjectOfType<BacgroundControllers>();
		backgroundController.Init(melody.Length, realitySprite, fantasySprite);
		phrasesSpawnRect = GameObject.Find("SpawnArea").GetComponent<RectTransform>();
		melodyGridHolder = GameObject.Find("Final Grid").GetComponent<GridLayoutGroup>();
		playMelodyButton = GameObject.Find("PlayMelody Button").GetComponent<Button>();
		playMelodyButton.onClick.RemoveAllListeners();
		playMelodyButton.onClick.RemoveListener(PlayMelody);
		
		playMelodyButton.onClick.AddListener(PlayMelody);

		isMelodyCorrect = false;

		SpawnMelodyPartHolders();
		SpawnPhrases();
		progressOfOnePhrase = 1 / (float) _melodyParts.Count;
	}

	private void SpawnPhrases()
	{
		foreach (var part in melody)
		{
			PhraseHolder phrase = Instantiate(phraseHolderPrefab, phrasesSpawnRect);
			phrase.transform.localPosition = GetRandomPosition(phrasesSpawnRect);

			phrase.Init(part, melodyClips[part]);
		}
	}

	private void SpawnMelodyPartHolders()
	{
		_melodyParts = new List<PartMelodyHolder>();
		foreach (var part in melody)
		{
			PartMelodyHolder phrase = Instantiate(melodyPartPrefab, melodyGridHolder.transform);
			phrase.Init(melodyClips[0], this);
			_melodyParts.Add(phrase);
		}
	}

	private Vector2 GetRandomPosition(RectTransform spawnRect)
	{
		Vector2 bottomLeft = spawnRect.rect.min;
		Vector2 topRight = spawnRect.rect.max;

		//topRight += spawnRect.rect.size / 2f;
		//bottomLeft -= spawnRect.rect.size / 2f;

		Vector2 result = new Vector2(Random.Range(bottomLeft.x, topRight.x), Random.Range(bottomLeft.y, topRight.y));
		return result;
	}

	private Coroutine playingEnum;

	public void PlayMelody()
	{
		Debug.Log("Play melody");
		StopPhrasePlaying();
		//StopMelodyPlaying();
		
		playMelodyButton.interactable = false;
		isMelodyCorrect = IsMelodyCorrect();
		if (isMelodyCorrect)
		{
			backgroundController.SetCompletedLevel();
			successBlocker.gameObject.SetActive(true);
			_melodyAudioSource.clip = fullMelody;
			_melodyAudioSource.Play();
		}
		playingEnum = StartCoroutine(PlayingMelody());
	}

	public void StopMelodyPlaying()
	{
		if (playingEnum != null)
		{
			StopCoroutine(playingEnum);
		}

		if (currentAudioSource != null)
		{
			currentAudioSource.Stop();
		}

		foreach (var item in _melodyParts)
		{
			item.transform.localScale = Vector3.one;
		}

		playMelodyButton.interactable = true;
	}

	public void StopPhrasePlaying()
	{
		if (currentlyPlayingPhrase != null)
		{
			currentlyPlayingPhrase.StopPlaying();
		}
	}

	private AudioSource currentAudioSource;

	private IEnumerator PlayingMelody()
	{
		for (int i = 0; i < _melodyParts.Count; i++)
		{
			var melodyPart = _melodyParts[i];
			if (melodyPart.attachedPhrase != null)
			{
				currentAudioSource = melodyPart.attachedPhrase.audioSource;
				melodyPart.attachedPhrase.Play(isMelodyCorrect);
				backgroundController.OnMelodyPartIsPlay(melodyPart.attachedPhrase.part == melody[i]);
			}
			else
			{
				currentAudioSource = _melodyAudioSource;
				currentAudioSource.clip = melodyPart.silentAudioClip;
				currentAudioSource.Play();
				backgroundController.OnMelodyPartIsPlay(false);
			}

			melodyPart.transform.DOScale(1.3f, 0.2f);

			while (currentAudioSource.isPlaying)
			{
				yield return null;
			}

			melodyPart.transform.DOScale(1f, 0.2f);
		}

		CheckTheResult();
	}

	public void CheckTheResult()
	{
		if (IsMelodyCorrect())
		{
			Debug.Log("YOU WIN");
			
			OnLevelCompleted();
		}
		else
		{
			Debug.Log("NOT YET");
		}
	}

	private bool IsMelodyCorrect()
	{
	//	PrintMelody();
		for (int i = 0; i < melody.Length - 1; i++)
		{
			if (_melodyParts[i].attachedPhrase == null || _melodyParts[i].attachedPhrase.part != melody[i])
			{
				return false;
			}
		}

		return true;
	}

	private void PrintMelody()
	{
		Debug.Log("melody now = ");
		string result = "melody now [";
		for (int i = 0; i < _melodyParts.Count; i++)
		{
			if (_melodyParts[i].attachedPhrase == null)
			{
				Debug.Log("now empty : " + melody[i]);
			}
			else
			{
				result += " " + _melodyParts[i].attachedPhrase.part + ", ";
				Debug.Log("now " + _melodyParts[i].attachedPhrase.part + " : " + melody[i]);
			}
		}

		Debug.Log("The end");
	}

	public void OnPlayPhrase(PhraseHolder phrase)
	{
		currentlyPlayingPhrase = phrase;
	}

	public void OnPhrasePlayingEnd(PhraseHolder phraseHolder)
	{
		if (currentlyPlayingPhrase == phraseHolder)
		{
			currentlyPlayingPhrase = null;
		}
	}


	public PhraseHolder currentlyDraggingPhrase;

	public void OnStartDraggingPhrase(PhraseHolder phraseHolder)
	{
		StopMelodyPlaying();
		StopPhrasePlaying();
		currentlyDraggingPhrase = phraseHolder;
	}

	public void OnEndDraggingPhrase()
	{
		currentlyDraggingPhrase = null;
	}

	public void OnLevelCompleted()
	{
		AppManager.Instance.StartWeeked();
	}
}