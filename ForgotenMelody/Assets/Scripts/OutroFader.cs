﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class OutroFader : MonoBehaviour
{
	public CanvasGroup fader, credits;
	public float targetAlpha, targetCredits = 0;

	// Use this for initialization
	void Start()
	{
		StartCoroutine(FaderCoroutine());
	}

	private IEnumerator FaderCoroutine()
	{
		yield return new WaitForSeconds(10f);
		targetAlpha = 1;
		
		yield return new WaitForSeconds(2f);

		targetCredits = 1;
	}

	void Update()
	{
		fader.alpha = Mathf.Lerp(fader.alpha, targetAlpha, Time.deltaTime);
		credits.alpha = Mathf.Lerp(credits.alpha, targetCredits, Time.deltaTime);
		
		
	}
}