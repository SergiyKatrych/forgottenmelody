﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PartMelodyHolder : MonoBehaviour
{
	private MelodyController _melodyController;
	private PhraseHolder currentPhrase;
	public PhraseHolder attachedPhrase;
	private Image _imageRef;

	public AudioClip silentAudioClip;

	public void Init(AudioClip melodyPart, MelodyController melodyController)
	{
		_melodyController = melodyController;
		silentAudioClip = melodyPart;
		_imageRef = GetComponent<Image>();
	}

	public void OnPointerEnter(BaseEventData eventData)
	{
		if (_melodyController.currentlyDraggingPhrase != null)
		{
			if (attachedPhrase != null)
			{
				_imageRef.color = Color.red;
			}
			else
			{
				_imageRef.color = Color.green;
				currentPhrase = _melodyController.currentlyDraggingPhrase;
				currentPhrase.GetComponent<DragableItem>().OnEnterHolder(this);
			}
		}
	}

	public void OnPointerExit(BaseEventData eventData)
	{
		_imageRef.color = Color.white;

		if (currentPhrase != null)
		{
			currentPhrase.GetComponent<DragableItem>().OnLeaveHolder(this);
//		if (currentPhrase == attachedPhrase)
//		{
//			attachedPhrase = null;
//		}
			currentPhrase = null;
		}
	}

	public void OnPointerUp(DragableItem itemOver)
	{
		Debug.Log("on pointer up " + gameObject.name);
		if (attachedPhrase == null)
		{
			attachedPhrase = itemOver.GetComponent<PhraseHolder>();
			attachedPhrase.DisablePlayButton();
			attachedPhrase.transform.position = transform.position;
			itemOver.OnStartDragging += () =>
			{
				attachedPhrase.EnablePlayButton();
				attachedPhrase = null;
				
				OnPointerEnter(null);
			};
		}

		currentPhrase = null;
	}
}