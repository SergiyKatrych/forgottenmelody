﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class PartyFader : MonoBehaviour
{
	private CanvasGroup currentImage;
	private Text textInImage;

	public AudioSource partySource, hangoverSource;
	public float soundFadingSpeed = 4;

	public float nightFadeDuration = 4;
	public float nightDuration = 5;

	public bool isVolumeUpSound, isVolumeDownSound;

	private float nightTargetProgress;

	public event Action onNightEnd;

	private Action onNightFaded;
	private bool isInited = false;

	void Init()
	{
		onNightEnd += () => { gameObject.SetActive(false); };
		currentImage = GetComponent<CanvasGroup>();
		textInImage = GetComponentInChildren<Text>();
		isInited = true;
	}

	public void StartNight(Action onNightFaded)
	{
		if (!isInited)
		{
			Init();
		}

		this.onNightFaded = onNightFaded;
		partySource.volume = 0;
		currentImage.alpha = 0;
		gameObject.SetActive(true);
		StartCoroutine(FadeInAnimation());
	}

	private IEnumerator FadeInAnimation()
	{
		nightTargetProgress = 1;
		isVolumeUpSound = true;
		isVolumeDownSound = false;
		yield return new WaitForSeconds(nightFadeDuration);

		isVolumeUpSound = false;
		if (onNightFaded != null)
		{
			onNightFaded();
		}
	}

	private IEnumerator FadeOutAnimation()
	{
		isVolumeUpSound = false;
		isVolumeDownSound = true;

		nightTargetProgress = 0;
		yield return new WaitForSeconds(nightFadeDuration*1.5f);
		hangoverSource.Play();
		onNightEnd();
	}

	void Update()
	{
		if (!nightTargetProgress.Equals(currentImage.alpha))
		{
			currentImage.alpha = Mathf.Lerp(currentImage.alpha, nightTargetProgress, Time.deltaTime);
		}

		if (isVolumeUpSound)
		{
			partySource.volume += Time.deltaTime * soundFadingSpeed;
		}

		if (isVolumeDownSound)
		{
			partySource.volume -= Time.deltaTime * soundFadingSpeed;
		}
	}

	public void StartFadeOutNight()
	{
		StartCoroutine(FadeOutAnimation());
	}
}