﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class PhraseHolder : MonoBehaviour
{
	public AudioSource audioSource;

	public AudioClip audioClip;

	public int part;

	private Coroutine playingCoroutine;

	private Button playButton;

	private MelodyController _melodyController;

	public void Play()
	{
		Play(false);
	}


	public void Play(bool isMelodyCorrect)
	{
		_melodyController.StopPhrasePlaying();
		if (playingCoroutine != null)
		{
			StopPlaying();
		}

		playingCoroutine = StartCoroutine(PlayCoroutine(isMelodyCorrect));
		_melodyController.OnPlayPhrase(this);
	}

	private IEnumerator PlayCoroutine(bool isMelodyCorrect)
	{
		transform.DOScale(1.2f, 0.1f);
		audioSource.clip = audioClip;
		audioSource.volume = isMelodyCorrect ? 0 : 1;
		audioSource.Play();
		while (audioSource.isPlaying)
		{
			yield return null;
		}

		StopPlaying();
	}

	public void StopPlaying()
	{
		Debug.Log("Stop Playing");
		if (audioSource.isPlaying)
		{
			audioSource.Stop();
		}

		transform.DOScale(1f, 0.1f);
		playingCoroutine = null;
		_melodyController.OnPhrasePlayingEnd(this);
	}

	public void Init(int part, AudioClip melodyPart)
	{
		audioSource = GetComponent<AudioSource>();
		_melodyController = FindObjectOfType<MelodyController>();
		playButton = transform.GetChild(0).GetComponent<Button>();
		audioClip = melodyPart;
		this.part = part;
	}

	public void EnablePlayButton()
	{
		playButton.interactable = true;
	}

	public void DisablePlayButton()
	{
		playButton.interactable = false;
	}
}