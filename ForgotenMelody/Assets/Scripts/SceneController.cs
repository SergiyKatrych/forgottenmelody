﻿using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour {

	public void StartMarchScene()
	{
		SceneManager.LoadScene("March");
	}
	
	public void StartSoadScene()
	{
		SceneManager.LoadScene("Soad");
	}

	public void StartKratosScene()
	{
		SceneManager.LoadScene("Kratos");
	}

	public void Start7NAScene()
	{
		SceneManager.LoadScene("7NA");
	}
}
